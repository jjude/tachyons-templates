# Tachyons Templates

Learning tachyons css by building different templates

## References

- [Tachyons Website](http://tachyons.io/)
- [Tachyons Cheatsheet](https://code.luasoftware.com/tutorials/web-development/tachyon-css-cheatsheet/)
- [Tachyons Templates Playground](https://www.tachyonstemplates.com/components/)
- [Codepen playground](https://codepen.io/inem/pen/WzLemG)

## Support
If you have any questions or comments, please reach out to me via [twitter](https://twitter.com/jjude)

## License
Use however you want it.

## Project status
It will never be complete. I will keep playing and adding to it. It might contain bugs too. So be cautious in using them as is.
